using MongoDB.Driver;
using NowPlaying.Models;
using NowPlaying.Utilities;
using SpotifyAPI.Web;

namespace NowPlaying.Spotify
{
    public class Auth : IAuth
    {
        private string CallbackUrl => $"{_config.BaseUri}:{_config.Port}/callback";
        private IMongoCollection<TokenModel> TokenCollection => _dbClient.GetDatabase(_config.MongoDatabase).GetCollection<TokenModel>("tokens");


        private readonly EnvironmentConfig _config;
        private readonly MongoClient _dbClient;

        public Auth(EnvironmentConfig config, MongoClient dbClient)
        {
            _config = config;
            _dbClient = dbClient;
        }

        public string LoginUrl()
        {
            var loginRequest = new LoginRequest(new Uri(CallbackUrl), _config.SpotifyClientId, LoginRequest.ResponseType.Code)
            {
                Scope = new[] { Scopes.UserReadCurrentlyPlaying, Scopes.UserReadPlaybackState, Scopes.UserReadPrivate }
            };

            return loginRequest.ToUri().ToString();
        }

        public async Task<TokenModel> Authorize(string code)
        {
            var response = await new OAuthClient().RequestToken(
                new AuthorizationCodeTokenRequest(_config.SpotifyClientId, _config.SpotifySecret, code, new Uri(CallbackUrl))
            );

            return await GenerateAndSaveToken(response);
        }

        public async Task<TokenModel> Reauthorize(string id)
        {
            var tokenModel = await GetToken(id);
            if (tokenModel is null)
            {
                throw new UnauthorizedAccessException("Token not found, user unauthorized.");
            }

            return await GenerateAndSaveToken(tokenModel.Token);
        }

        public async Task Logout(string id)
        {
            await DeleteToken(id);
        }

        private async Task UpsertToken(TokenModel token)
        {
            var filter = Builders<TokenModel>.Filter.Eq(x => x.Id, token.Id);

            await TokenCollection.ReplaceOneAsync(filter, token, new ReplaceOptions { IsUpsert = true });
        }

        private async Task<TokenModel> GetToken(string id)
        {
            var filter = Builders<TokenModel>.Filter.Eq(x => x.Id, id);

            return await (await TokenCollection.FindAsync(filter)).FirstOrDefaultAsync();
        }

        private async Task DeleteToken(string id)
        {
            var filter = Builders<TokenModel>.Filter.Eq(x => x.Id, id);

            await TokenCollection.DeleteOneAsync(filter);
        }

        private async Task<TokenModel> GenerateAndSaveToken(AuthorizationCodeTokenResponse tokenResponse)
        {
            var client = new SpotifyClient(SpotifyClientConfig.CreateDefault().WithAuthenticator(
                new AuthorizationCodeAuthenticator(_config.SpotifyClientId, _config.SpotifySecret, tokenResponse)
            ));

            var profile = await client.UserProfile.Current();
            var token = new TokenModel { Id = profile.Id.ToBase64(), Token = tokenResponse };

            await UpsertToken(token);

            return token;
        }
    }
}