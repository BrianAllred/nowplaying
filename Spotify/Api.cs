using MongoDB.Driver;
using NowPlaying.Models;
using NowPlaying.Utilities;
using SpotifyAPI.Web;

namespace NowPlaying.Spotify
{
    public class Api : IApi
    {
        private IMongoCollection<TrackModel> TrackCollection => _dbClient.GetDatabase(_config.MongoDatabase).GetCollection<TrackModel>("tracks");

        private readonly MongoClient _dbClient;
        private readonly IAuth _auth;
        private readonly EnvironmentConfig _config;

        public Api(EnvironmentConfig config, MongoClient dbClient, IAuth auth)
        {
            _dbClient = dbClient;
            _auth = auth;
            _config = config;
        }

        public async Task<FullTrack> NowPlayingTrack(string id)
        {
            var tokenModel = await _auth.Reauthorize(id);
            var client = new SpotifyClient(SpotifyClientConfig.CreateDefault().WithAuthenticator(
                new AuthorizationCodeAuthenticator(_config.SpotifyClientId, _config.SpotifySecret, tokenModel.Token)
            ));

            var context = await client.Player.GetCurrentPlayback();

            if (context is null)
            {
                throw new NullReferenceException("No playback context, user not playing music.");
            }

            if (context.Item.Type == ItemType.Track)
            {
                var newTrackModel = new TrackModel { Id = id, Track = (FullTrack)context.Item };
                await UpsertTrack(newTrackModel);
                return newTrackModel.Track;
            }

            return (await GetTrack(id)).Track;
        }

        private async Task UpsertTrack(TrackModel track)
        {
            var filter = Builders<TrackModel>.Filter.Eq(x => x.Id, track.Id);

            await TrackCollection.ReplaceOneAsync(filter, track, new ReplaceOptions { IsUpsert = true });
        }

        private async Task<TrackModel> GetTrack(string id)
        {
            var filter = Builders<TrackModel>.Filter.Eq(x => x.Id, id);

            return await (await TrackCollection.FindAsync(filter)).FirstAsync();
        }
    }
}