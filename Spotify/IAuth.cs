using NowPlaying.Models;

namespace NowPlaying.Spotify
{
    public interface IAuth
    {
        public string LoginUrl();
        public Task<TokenModel> Authorize(string code);
        public Task<TokenModel> Reauthorize(string id);
        public Task Logout(string id);
    }
}