using SpotifyAPI.Web;

namespace NowPlaying.Spotify
{
    public interface IApi
    {
        public Task<FullTrack> NowPlayingTrack(string id);
    }
}