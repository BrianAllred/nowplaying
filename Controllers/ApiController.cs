using Microsoft.AspNetCore.Mvc;
using NowPlaying.Spotify;

namespace NowPlaying.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiController : ControllerBase
    {
        private readonly ILogger<ApiController> _logger;
        private readonly IAuth _auth;
        private readonly IApi _api;

        public ApiController(IApi api, IAuth auth, ILogger<ApiController> logger)
        {
            _logger = logger;
            _auth = auth;
            _api = api;
        }

        [HttpGet]
        [Route("loginurl")]
        public IActionResult GetLoginUrl()
        {
            return Ok(_auth.LoginUrl());
        }

        [HttpPost]
        [Route("callback")]
        public async Task<IActionResult> PostCallback([FromBody] string code)
        {
            return Ok((await _auth.Authorize(code)).Id);
        }

        [HttpPost]
        [Route("logout")]
        public async Task<IActionResult> PostLogout([FromBody] string id)
        {
            await _auth.Logout(id);
            return Ok();
        }

        [HttpGet]
        [Route("nowplaying/{id}")]
        public async Task<IActionResult> GetNowPlaying([FromRoute] string id)
        {
            try
            {
                var track = await _api.NowPlayingTrack(id);
                return Ok(track);
            }
            catch (UnauthorizedAccessException ex)
            {
                return Unauthorized(ex.Message);
            }
            catch (NullReferenceException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet]
        [Route("nowplaying/text/{id}")]
        public async Task<IActionResult> GetNowPlayingText([FromRoute] string id)
        {
            try
            {
                var track = await _api.NowPlayingTrack(id);
                return Ok($"{track.Artists.First().Name} - {track.Name}");
            }
            catch (UnauthorizedAccessException ex)
            {
                return Unauthorized(ex.Message);
            }
            catch (NullReferenceException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}