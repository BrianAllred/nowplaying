FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app

# Install Node
RUN apt-get update -yq
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -yq nodejs

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out
WORKDIR /app/Vue
RUN npm ci
RUN npm run build

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build /app/out ./
COPY --from=build /app/Vue/dist ./wwwroot/

EXPOSE 80

# Override these during deployment
ENV MONGO_SERVER=localhost
ENV MONGO_PORT=27017
ENV MONGO_DATABASE=spotify
ENV MONGO_USERNAME=nowplaying
ENV MONGO_PASSWORD=spotify
ENV SPOTIFY_CLIENTID=
ENV SPOTIFY_SECRET=
ENV BASE_URI="http://localhost"
ENV PORT="5225"

ENTRYPOINT ["dotnet", "NowPlaying.dll"]
