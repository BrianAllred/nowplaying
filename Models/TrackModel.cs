using SpotifyAPI.Web;

namespace NowPlaying.Models
{
    public class TrackModel
    {
        public string Id { get; set; }
        public FullTrack Track { get; set; }
    }
}