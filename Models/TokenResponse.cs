using SpotifyAPI.Web;

namespace NowPlaying.Models
{
    public class TokenModel
    {
        public string Id { get; set; }
        public AuthorizationCodeTokenResponse Token { get; set; }
    }
}