using MongoDB.Driver;
using NowPlaying.Spotify;
using NowPlaying.Utilities;

var config = new EnvironmentConfig();

var mongoSettings = new MongoClientSettings();
mongoSettings.Server = new MongoServerAddress(config.MongoServer, int.Parse(config.MongoPort));
mongoSettings.Credential = MongoCredential.CreateCredential(config.MongoDatabase, config.MongoUsername, config.MongoPassword);
var mongoClient = new MongoClient(mongoSettings);

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSpaStaticFiles(options => { options.RootPath = "wwwroot"; });
builder.Services.AddMvc(option => option.EnableEndpointRouting = false);
builder.Services.AddCors(options =>
{
    options.AddPolicy("VueCorsPolicy", builder =>
    {
        builder
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
        .WithOrigins("http://localhost:5225");
    });
});

builder.Services.AddSingleton<MongoClient>(mongoClient);
builder.Services.AddSingleton<EnvironmentConfig>(config);
builder.Services.AddScoped<IAuth, Auth>();
builder.Services.AddScoped<IApi, Api>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();
app.UseMvc();
app.UseAuthorization();
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
app.UseCors("VueCorsPolicy");
app.UseSpaStaticFiles();
app.UseSpa(builder =>
{
    if (app.Environment.IsDevelopment())
    {
        builder.UseProxyToSpaDevelopmentServer("http://localhost:8080");
    }
});


app.Run();
