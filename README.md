# NowPlaying

[![pipeline status](https://gitlab.com/BrianAllred/nowplaying/badges/main/pipeline.svg)](https://gitlab.com/BrianAllred/nowplaying/-/commits/main)

## Description

A tool to overlay your currently playing Spotify track info on your stream, VOD, or other media.

## Showcase

![Example GIF](https://gitlab.com/BrianAllred/nowplaying/-/raw/media/gifs/example.gif)

## Installation and Usage

### Register application with Spotify

- Register a new application in the [Spotify developer dashboard](https://developer.spotify.com/dashboard/applications).
- Edit the application settings to add your website to the Redirect URIs. For example, if your website is at `https://nowplaying.example.com`, you would add `https://nowplaying.example.com/callback`.
- Click show client secret (in a safe and secure environment).
- Copy the Client ID and the secret to a safe location.

### Set up application

**Easy setup:**

The docker example or something similar is highly recommended as it greatly reduces set up complexity.

Set environment variables in the `docker-compose.yml` as appropriate, then run with `docker-compose up -d`.

**Manual setup:**

- Install and deploy MongoDB.
- Install Dotnet 6 runtime for your system.
- Download the [appropriate artifact](https://gitlab.com/BrianAllred/nowplaying/-/releases) for your system.
- Set appropriate environment variables (manually or via script), then run the `NowPlaying` executable.

## Support

Bug reports are more than welcome! Feel free to create new issues for any bugs encountered.

## Roadmap

- [ ] Implement API throtting where appropriate
- [ ] Add better backend error handling

## Contributing

Contributions are also more than welcome. Development environment consists of

- Node 16.x LTS
- Dotnet 6 SDK
- Latest MongoDB
- Docker (recommended)
- VS Code (recommended)

Please use Prettier (or compatible) formatter if submitting pull requests.

### Recommended development setup

- Remove the `nowplaying` service from the example `docker-compose.yml` file.
- Add `ports` to the `mongo` service. Default is `27017:27017`, but edit as appropriate.
  - Note that if you change the default mongo port, add a `command: --port <port>` to the `mongo` service.
- Run `docker-compose up -d`.
- Set environment variables in `launchSettings.json` as appropriate.
- In the `Vue` directory, run `npm ci && npm run serve`.
- Debug (F5) the project using VS Code. This should open a browser to the main page.

## License

MIT
