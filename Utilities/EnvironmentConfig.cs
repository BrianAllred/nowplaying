namespace NowPlaying.Utilities
{
    public class EnvironmentConfig
    {
        public string MongoServer => Environment.GetEnvironmentVariable("MONGO_SERVER");
        public string MongoPort => Environment.GetEnvironmentVariable("MONGO_PORT");
        public string MongoDatabase => Environment.GetEnvironmentVariable("MONGO_DATABASE");
        public string MongoUsername => Environment.GetEnvironmentVariable("MONGO_USERNAME");
        public string MongoPassword => Environment.GetEnvironmentVariable("MONGO_PASSWORD");
        public string SpotifyClientId => Environment.GetEnvironmentVariable("SPOTIFY_CLIENTID");
        public string SpotifySecret => Environment.GetEnvironmentVariable("SPOTIFY_SECRET");
        public string BaseUri => Environment.GetEnvironmentVariable("BASE_URI");
        public string Port => Environment.GetEnvironmentVariable("PORT");
    }
}