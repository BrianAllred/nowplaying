import axios from "axios";
import { createApp } from "vue";
import { VueCookieNext } from "vue-cookie-next";
import App from "./App.vue";
import router from "./router";
import PrimeVue from "primevue/config";
import store from "./store";

import "primevue/resources/themes/luna-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";

import "@fontsource/metropolis";

axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.baseURL = process.env.BASE_URL;

createApp(App)
  .use(PrimeVue, { ripple: true })
  .use(VueCookieNext)
  .use(store)
  .use(router)
  .mount("#app");
