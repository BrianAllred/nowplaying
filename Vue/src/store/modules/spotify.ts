import { api } from "@/api";
import { ApiError, SimpleTrack } from "@/types/interfaces";
import { Track } from "spotify-web-api-ts/types/types/SpotifyObjects";
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

@Module({ name: "SpotifyState" })
export class SpotifyState extends VuexModule {
  track: SimpleTrack = {};
  loading = false;
  userId = "";
  text = "";
  hasError = false;
  errorCode = 0;
  intervalId = 0;

  fullTrack!: Track | null;

  @Mutation
  private setLoading(loading: boolean) {
    this.loading = loading;
  }

  @Mutation
  private setId(id: string) {
    this.userId = id;
  }

  @Mutation
  private setTrack(track: Track | null) {
    this.fullTrack = track;

    if (track === null) {
      this.track = {};
      return;
    }

    this.track = {
      id: track.id,
      name: track.name,
      albumId: track.album.id,
      albumImgSrc: track.album.images[0].url,
      artists: track.artists.map((a) => a.name),
    };
  }

  @Mutation
  private setError(error: ApiError) {
    this.errorCode = error.code ?? 200;
    this.text = error.text ?? "";
    this.hasError = this.errorCode >= 400 || this.text !== "";
  }

  @Mutation
  private setIntervalId(id: number) {
    this.intervalId = id;
  }

  @Action
  public async getNowPlaying(): Promise<void> {
    if (this.loading) return;
    this.setLoading(true);
    await api
      .getNowPlaying(this.userId)
      .then(async (track) => {
        this.setError({});
        if (this.track && this.track.id !== track.id) {
          this.setTrack(track);
        }
      })
      .catch(async (err) => {
        this.setTrack(null);
        const errorRequest = err.request as XMLHttpRequest;
        if (errorRequest) {
          this.setError({
            code: errorRequest.status,
            text: errorRequest.responseText,
          });
        }
      })
      .finally(() => this.setLoading(false));
  }

  @Action
  public setUserId(id: string): void {
    this.setId(id);
  }

  @Action
  public activateTimer(id: string): void {
    this.setId(id);
    this.getNowPlaying();
    this.setIntervalId(setInterval(this.getNowPlaying, 5000));
  }

  @Action
  public stopTimer(): void {
    clearInterval(this.intervalId);
  }
}
