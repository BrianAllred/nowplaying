import Vuex from "vuex";
import { getModule } from "vuex-module-decorators";
import { SpotifyState } from "./modules/spotify";

const store = new Vuex.Store({
  modules: {
    SpotifyState,
  },
});

export default store;
export const SpotifyModule = getModule(SpotifyState, store);
