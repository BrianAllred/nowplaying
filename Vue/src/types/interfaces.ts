export interface SimpleTrack {
  albumImgSrc?: string;
  artists?: string[];
  name?: string;
  id?: string;
  albumId?: string;
}

export interface ApiError {
  text?: string;
  code?: number;
}
