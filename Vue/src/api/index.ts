import axios from "axios";
import { Track } from "spotify-web-api-ts/types/types/SpotifyObjects";

class Api {
  private readonly api: string = `api/`;

  public async getLoginUrl(): Promise<string> {
    const result = await axios.get<string>(`${this.api}loginurl`);
    return result.data;
  }

  public async postCallback(code: string): Promise<string> {
    const result = await axios.post<string>(`${this.api}callback`, `"${code}"`);
    return result.data;
  }

  public async getNowPlaying(id: string): Promise<Track> {
    const result = await axios.get<Track>(`${this.api}nowplaying/${id}`);
    return result.data;
  }

  public async getNowPlayingText(id: string): Promise<string> {
    const result = await axios.get<string>(`${this.api}nowplaying/text/${id}`);
    return result.data;
  }

  public async postLogout(id: string): Promise<void> {
    await axios.post(`${this.api}logout`, `"${id}"`);
  }
}

export const api = new Api();
